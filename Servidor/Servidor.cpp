#include <stdio.h>
#include <winsock.h> // Inclui o header do winsock.
#include <conio.h>
#include <string.h>
#include <thread>

// Esta e uma alternativa para especificar a biblioteca no campo de dependencias externas nas configuracoes do projeto.
#pragma comment(lib, "ws2_32.lib")

// Variaveis que recebem o valor da inicializa��o e cria��o do socket.
WSADATA data;
SOCKET winsock;

// estrutura derivada da sockaddr especializada para trabalhar com o protocolo TCP / IP.
// Struct -> contem a configuracao do socket a ser usado o ip, a porta e a familia do socket.
SOCKADDR_IN sock;

/*
struct sockaddr_in
{
	short sin_family;  // define o protocolo de transporte, como no caso � TCP ou UDP o utilizado � �AF_INET�;
	u_short sin_port;  // o n�mero da porta a ser utilizada nesse processo;
	struct in_addr sin_addr;  // o endere�o IP;
	char sin_zero[8];  // n�o � utilizado;
};
*/

void atenderCliente(SOCKET winsock, int numCliente) {

	printf("\nNova Conexao Estabelecida com o cliente[ %i ]", numCliente);
	char msgSaida[10];
	// converte o numero do cliente para char
	sprintf_s(msgSaida, "%d", numCliente);

	// envia ao cliente seu id de identifica��o
	send(winsock, msgSaida, strlen(msgSaida), 0);

	// vari�vel para controle de conex�o com o cliente
	int bytes;

	// permanece no loop enquanto houver conexao com o cliente
	while (true) {
		char msgEntrada[1024];
		// Recebendo dados
		// Enviando dados:
		// recv(SOCKET, BUFFER, TAMANHO, FLAGS);
		// SOCKET-> nosso socket que foi criado;
		// BUFFER-> dados a enviar;
		// TAMANHO-> tamanho dos dados;
		// FLAGS -> como a fun��o se comporta. Dificilmente ser� utilizado um valor diferente de zero
		bytes = recv(winsock, msgEntrada, 1024, 0);

		// fun��o recv retorna -1 se a conex�o com o cliente for terminada
		// caso a condi��o do if seja falsa, segue o fluxo
		if (bytes == -1) {
			// exibe mensagem para o usu�rio
			printf("\nConexao perdida com o cliente[ %i ] ", numCliente);

			// termina o Loop
			break;
		}

		// exibe mensagem recebida do socket
		printf("\ncliente[ %i ] : %s", numCliente, msgEntrada);

		// retorna para a funcao
	}

	// Fica esperando at� ser precionado uma tecla do teclado e termina imediatamente
	///_getch();

	// fechar o socket
	closesocket(winsock);
	printf("Socket Fechado!!!\n");
}

int main() {
	// identificador do cliente
	// incrementado a cada nova conex�o
	int numCliente = 1;

	// Inicializa o uso do winsock. Retorna o valor 0 (zero)quando � finalizada com sucesso ou - 1, quando um erro ocorre.

	// inicializa o winsocks(implementa��o de sockets do Windows) e verifica se o computador suporta a vers�o do winsocks a ser utilizada.
	// Sintaxe : WSAStartup(VERSAO_DO_WINSOCK, ENDERE�O DA VARI�VEL)
	// Parametros:
		// 1- deve ser fornecida a vers�o do winsocks a ser utilizada.
		// 2- � enviado um ponteiro para uma estrutura WSADATA, a qual receber� os detalhes da implementa��o do socket.
	if (WSAStartup(MAKEWORD(1, 1), &data) == SOCKET_ERROR) {
		printf("ERRO - Falha ao iniciar o Socket");
		// sistema fica esperando alguma tecla ser pressionada, para o usu�rio poder visualizar a mensagem de erro
		system("pause");
		return 0;
	}

	// criara um socket para determinado tipo de protocolo de transferencia
	// Sintaxe : socket(FAMILIA, TIPO_DE_PROTOCOLO, PROTOCOLO); 
	// Parametros:
		// 1- deve ser fornecido o tipo de endere�o utilizado. Para protocolo TCP ou UDP utilizamos �AF_INET�
		// 2- define-se o tipo de socket a ser criado. �SOCK_STREAM� para criar um socket TCP, e �SOCK_DGRAM� para um socket UDP.
		// 3- define-se o protocolo a ser utilizado no socket. �IPPROTO_TCP� para utilizar o protocolo TCP.
	if ((winsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == SOCKET_ERROR) {
		printf("ERRO - Falha ao criar o Socket");
		system("pause");
		return 0;
	}
	printf("\nSocket Iniciado com sucesso\n");

	// Estrutura SOCKADDR_IN definida nas variaveis globais
	sock.sin_family = AF_INET;
	sock.sin_port = htons(7008);


	// Fun��o BIND tamb�m retorna um valor, que por sinal, s�o os mesmos.
	// Esta fun��o � respons�vel por colocar o socket no modo BIND(configur�-lo na porta local) (bind == ligar).
	// atribui a um socket um endere�o IP e uma porta que foram anteriormente definidas numa estrutura sockaddr_in.
	// Parametros:
		// 1- coloca-se o socket a receber o endere�o IP e a porta.
		// 2- coloca-se o sockaddr_in(ipv4) (sockaddr_in6(ipv6)) criado anteriormente. O �nico detalhe � que se faz necess�rio fazer uma convers�o para sockaddr(geral).
	if (bind(winsock, (SOCKADDR*)&sock, sizeof(sock)) == SOCKET_ERROR) {
		printf("ERRO - Falha ao utilizar a fun��o Bind");
		system("pause");
		return 0;
	}

	// Coloca o socket em modo de espera, aceitando 100 conex�es
	// coloca o socket(j� configurado) em estado de escuta. 
	// Parametros:
		// 1- socket que ser� colocado em estado de escuta;
		// 2- de sockets que poder�o se conectar ao sistema.
	listen(winsock, 100);

	printf("\nEsperando Conexao\n");

	// verifica se houve um pedido de conex�o, como a fun��o ACCEPT retorna -1 quando n�o h� pedidos, 
	// o loop ser� feito at� que o retorno seja diferente de -1, que indica um pedido de conex�o.

	while (true) {

		// sockaddr_in sInfo;
		// int infSize = sizeof(sInfo);

		// utilizada ap�s um socket ser colocada em estado de escuta e h� uma conex�o pendente nele.
		// Essa fun��o aceita a conex�o feita por um outro computador(cliente) e retorna o socket com as informa��es recebidas.
		// PARAMETROS
			// 1- socket que est� em estado de escuta.
			// 2- ponteiro para uma estrutura addr_in, contendo as informa��es do computador remoto.
			// 3- tamanho da estrutura addr_in presente no segundo par�metro dessa fun��o.
		SOCKET newSocket = accept(winsock, 0, 0);

		if (newSocket < 0) {
			printf("\nErro ao aceitar conexao");
		}

		// cria uma nova thread para antendimento do cliente
		// PARAMETROS
			// 1- funcao responsavel pelo atendimento
			// 2- novo socket contendo os dados do cliente
			// 3- identificacao do cliente
		std::thread atender(atenderCliente, newSocket, numCliente);

		// "desliga" a thread da thread principal, permitindo que ambos executem concorrentemente
		atender.detach();

		numCliente++;
	}

	// enquanto o programa estiver rodando, estaremos aptos a criar novos sockets. 
	// Ao finalizar um programa que utiliza sockets, utilizamos sempre WSACleanup() para encerrar o uso do winsock.
	WSACleanup();

	return 0;
}